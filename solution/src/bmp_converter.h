#pragma once
#include "bmp_header.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>


enum statusRead {
    READ_OKEY = 0,
    READ_INVALID_IMAGE_TYPE,
    READ_INVALID_IMAGE_BIT,
    READ_INVALID_BMP_HEAD,
    MALLOC_ERROR
};

enum statusWrite {
    WRITE_OKEY = 0,
    WRITE_BMP_ERROR
};

enum statusRead from_bmp(FILE *in, struct image *img);

enum statusWrite to_bmp(FILE *out, struct image const *img);
