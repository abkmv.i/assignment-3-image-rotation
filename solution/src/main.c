#include "bmp_converter.h"
#include "file.h"
#include "image_converter.h"

#define ZERO 0
#define OPEN 1
#define OPEN_FILE 2

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    if (argc != 3) {
        fprintf(stderr, "imageTransformer should only take 2 parameters.\n");
        return ZERO;
    }

    FILE *input = fileInput(argv[OPEN], "r");
    if (input == NULL) {
        fprintf(stderr, "The source file cannot be opened. \n");
        return -1;
    }

    FILE *output = fileInput(argv[OPEN_FILE], "w");
    if (output == NULL) {
        fprintf(stderr, "The target file cannot be opened.. \n");
        return -1;
    }

    struct image image = {0};
    enum statusRead statusRead = from_bmp(input, &image);
    if (statusRead == READ_OKEY) {
        struct image rotated = rotate(image);
        enum statusWrite statusWrite = to_bmp(output, &rotated);
        if (statusWrite == WRITE_OKEY) {
            fprintf(stdout, "Operation is successful \n");
            free(rotated.data);
            free(image.data);
            fclose(input);
            fclose(output);
        } else if (statusWrite == WRITE_BMP_ERROR) {
            fprintf(stderr, "An error occurred while writing to file\n");
            free(rotated.data);
            free(image.data);
            fclose(input);
            fclose(output);
            return -1;
        }
        return ZERO;
    } else {
        if (statusRead == READ_INVALID_BMP_HEAD) {
            fprintf(stderr, "Wrong image header received \n");
        } else if (statusRead == READ_INVALID_IMAGE_TYPE) {
            fprintf(stderr, "Wrong image type received \n");
        } else if (statusRead == READ_INVALID_IMAGE_BIT) {
            fprintf(stderr, "Bad bits in file \n");
        }
        free(image.data);

        fileClose(input);
        fileClose(output);

        return -1;
    }

}
