//
// Created by user on 13.03.2023.
//
#include "file.h"

void fileClose(FILE* file) {
    fclose(file);
}

FILE* fileInput(const char *name, const char *mode) {
    FILE *input;
    if ((input = fopen(name, mode)) == NULL) {
        return NULL;
    }
    return input;
}

