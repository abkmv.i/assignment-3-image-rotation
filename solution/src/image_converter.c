//
// Created by user on 13.03.2023.
//
#include "image_converter.h"
#include <stdio.h>

#define PIXEL_SIZE 3

static uint32_t coordinatesToAddress(uint32_t row, uint32_t img_column, uint32_t img_width) {
    return row * img_width + img_column;
}

struct image rotate( struct image const img ) {
    struct image img_rotated = {0};
    int32_t img_width = (int32_t) img.width;
    int32_t img_height = (int32_t) img.height;
    img_rotated.data = (struct pixel *) malloc(img_width * img_height * PIXEL_SIZE);
    if (img_rotated.data == NULL) {
        fprintf(stderr, "Memory full \n");
        abort();
    }
    img_rotated.height = img_width;
    img_rotated.width = img_height;
    for (int32_t img_column = 0; img_column < img_width; img_column++) {
        for (int32_t img_row = img_height - 1; img_row >= 0; img_row--) {
            img_rotated.data[img_column * img_height + (img_height - 1 - img_row)] =
                    img.data[coordinatesToAddress(img_row, img_column, img_width)];
        }
    }
    return img_rotated;
}
