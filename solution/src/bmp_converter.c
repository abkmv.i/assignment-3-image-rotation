#include "bmp_converter.h"

#define BMP_IMAGE_TYPE 0x4d42
#define BMP_IMAGE_DATA_PIXEL 54
#define BMP_SIZE_HEAD 40
#define BMP_PLANES 1
#define BMP_IMAGE_BITS_PIXEL 24
#define BMP_IMAGE_COMPRESSION 0
#define PIXEL_SIZE_IN_IMAGE 3
#define BMP_HEAD_PADDING 4
#define ZERO 0

static uint32_t paddingCal(uint32_t img_width) {
    return (BMP_HEAD_PADDING - (img_width * PIXEL_SIZE_IN_IMAGE % BMP_HEAD_PADDING)) % 4;
}

static uint32_t ImageSizeCal(uint32_t img_width, uint32_t img_height) {
    return (PIXEL_SIZE_IN_IMAGE * img_width + paddingCal(img_width)) * img_height;
}

static struct bmp_header headersFromImage(struct image const *current_img) {
    uint32_t bmp_content_size = ImageSizeCal(current_img->width, current_img->height);
    return (struct bmp_header) {
            .bfType = BMP_IMAGE_TYPE,
            .bfileSize = BMP_IMAGE_DATA_PIXEL + bmp_content_size,
            .bfReserved = ZERO,
            .bOffBits = BMP_IMAGE_DATA_PIXEL,
            .biSize = BMP_SIZE_HEAD,
            .biWidth = current_img->width,
            .biHeight = current_img->height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_IMAGE_BITS_PIXEL,
            .biCompression = BMP_IMAGE_COMPRESSION,
            .biSizeImage = bmp_content_size,
            .biXPelsPerMeter = ZERO,
            .biYPelsPerMeter = ZERO,
            .biClrUsed = ZERO,
            .biClrImportant = ZERO
    };
}

static uint32_t addressFromCoordinates(uint32_t img_width, uint32_t img_padding, uint32_t img_pixel_row, uint32_t img_pixel_column) {
    return ((img_width * PIXEL_SIZE_IN_IMAGE + img_padding) * (img_pixel_row) + img_pixel_column * PIXEL_SIZE_IN_IMAGE);
}
static int8_t checkedHeaderBmp(struct bmp_header* bmp_header) {
    if (ImageSizeCal(bmp_header->biWidth, bmp_header->biHeight) > bmp_header->biSizeImage) return ZERO;
    if (bmp_header->bfileSize - bmp_header->biSizeImage - BMP_SIZE_HEAD < ZERO) return ZERO;
    if (bmp_header->bfType != BMP_IMAGE_TYPE) return ZERO;
    if (bmp_header->biPlanes != BMP_PLANES) return ZERO;
    if (bmp_header->biBitCount != BMP_IMAGE_BITS_PIXEL) return ZERO;
    if (bmp_header->biCompression != BMP_IMAGE_COMPRESSION) return ZERO;
    if (bmp_header->biSize != BMP_SIZE_HEAD) return ZERO;
    if (bmp_header->bOffBits != BMP_IMAGE_DATA_PIXEL) return ZERO;
    if (bmp_header->bfReserved != ZERO) return ZERO;
    return 1;
}

enum statusWrite to_bmp(FILE *out, struct image const* img) {
    struct bmp_header bmp_headers = headersFromImage(img);
    if (fwrite(&bmp_headers, BMP_SIZE_HEAD, 1, out) != 1) {
        return WRITE_BMP_ERROR;
    }
    uint32_t img_width = img->width;
    uint32_t img_height = img->height;
    uint32_t img_size = bmp_headers.biSizeImage;
    uint32_t img_padding = paddingCal(img_width);
    uint8_t *bmp_data = (uint8_t *) malloc(img_size);
    if (bmp_data == NULL) {
        fprintf(stderr, "Memory full \n");
        abort();
    }
    for (uint32_t bit = ZERO; bit < img_size; bit++) {
        bmp_data[bit] = ZERO;
    }
    for (uint32_t img_row = ZERO; img_row < img_height; img_row++) {
        for (uint32_t img_column = ZERO; img_column < img_width; img_column++) {
            uint32_t initial_address = addressFromCoordinates(img_width, img_padding, img_row,
                                                              img_column);
            uint32_t target_address = img_row * img_width + img_column;
            bmp_data[initial_address + ZERO] = img->data[target_address].b;
            bmp_data[initial_address + 1] = img->data[target_address].g;
            bmp_data[initial_address + 2] = img->data[target_address].r;
        }
    }
    fseek(out, BMP_IMAGE_DATA_PIXEL, ZERO);
    if (fwrite(bmp_data, 1, img_size, out) != img_size) {
        free(bmp_data);
        return WRITE_BMP_ERROR;
    }
    free(bmp_data);
    return WRITE_OKEY;
}

enum statusRead from_bmp(FILE *in, struct image* img) {
    struct bmp_header bmp_headers = {ZERO};
    fseek(in, ZERO, ZERO);
    if (fread(&bmp_headers, BMP_SIZE_HEAD, 1, in) != 1) {
        return READ_INVALID_IMAGE_TYPE;
    }
    if (!checkedHeaderBmp(&bmp_headers)) {
        return READ_INVALID_BMP_HEAD;
    }
    uint32_t img_height = bmp_headers.biHeight;
    uint32_t img_width = bmp_headers.biWidth;
    uint32_t img_size = bmp_headers.biSizeImage;
    uint32_t img_padding = paddingCal(img_width);

    img->width = img_width;
    img->height = img_height;
    img->data = (struct pixel *) malloc(img_width * img_height * PIXEL_SIZE_IN_IMAGE);
    uint8_t *bmp_data = (uint8_t *) malloc(img_size);
    if (img->data == NULL || bmp_data == NULL) {
        fprintf(stderr, "Memory full \n");
        abort();
    }
    fseek(in, BMP_IMAGE_DATA_PIXEL, ZERO);
    if (fread(bmp_data, img_size, 1, in) != 1) {
        free(bmp_data);
        free(img->data);
        return READ_INVALID_IMAGE_BIT;
    }
    for (uint32_t img_row = ZERO; img_row < img_height; img_row++) {
        for (uint32_t img_column = ZERO; img_column < img_width; img_column++) {
            uint32_t initial_address = img_row * img_width + img_column;
            uint32_t target_address = addressFromCoordinates(img_width, img_padding,
                                                             img_row, img_column);
            img->data[initial_address] = (struct pixel) {
                    bmp_data[target_address], bmp_data[target_address + 1], bmp_data[target_address + 2]
            };
        }
    }
    free(bmp_data);
    return READ_OKEY;
}
