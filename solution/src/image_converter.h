//
// Created by user on 13.03.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_CONVERTER_H
#define IMAGE_TRANSFORMER_IMAGE_CONVERTER_H
#pragma once
#include "image.h"
#include <stdlib.h>

struct image rotate( struct image const img );

#endif //IMAGE_TRANSFORMER_IMAGE_CONVERTER_H
