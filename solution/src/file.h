//
// Created by user on 13.03.2023.
//

#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H
#pragma once
#include <stdio.h>
#include <stdlib.h>

FILE* fileInput(const char* name, const char* mode);
void fileClose(FILE* file);

#endif //IMAGE_TRANSFORMER_FILE_H
